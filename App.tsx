import { ThemeProvider } from 'styled-components/native';
import light from './src/theme/light';
import { StatusBar } from 'expo-status-bar';
import Home from './src/screens/Home';

export default function App() {
  return (
  <ThemeProvider theme={light}>
    <StatusBar style='auto'/>
    <Home />
  </ThemeProvider>

  );
}

