import { Text, View } from "react-native";
import * as S from "./styles";

import wallet from '../../assets/img/Wallet.png';


export default function CartaoDetalhes(){
    return(
        <S.Container>
            <S.Topo>
                <S.SaldoTxt>
                    Saldo disponivel
                </S.SaldoTxt>
                <S.CarteiraImg source={wallet}/>
                
                
            </S.Topo>
            
                <S.ValorTxt>
                    R$145,76
                </S.ValorTxt>
            
            

        </S.Container>
    )
}