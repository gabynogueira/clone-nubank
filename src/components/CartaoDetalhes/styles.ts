import styled from "styled-components/native";

export const Container = styled.View`
    width: 100%;
    height: 120px;
    margin-bottom: 20px;
    padding: 20px;
    border-radius: 20px;
    justify-content: space-around;
    
    background: ${({ theme })=> theme.COLORS.BACKGROUND_CARTAO};
    
`;

export const Topo = styled.View`
    flex-direction: row;
    justify-content: space-between;

    
`;
export const SaldoTxt = styled.Text`
    font-size: 14px;
    font-weight: 600;

     color: ${({theme})=>theme.COLORS.TEXT};
`;
export const CarteiraImg = styled.Image`

`;

export const ValorTxt = styled.Text`
    font-size: 34px;
    font-weight: 600;
    
    color: ${({theme})=>theme.COLORS.TEXT};
`;
