import { Text, View } from "react-native";
import * as S from "./styles";

import logo from '../../assets/img/logo.png'
import setting from '../../assets/img/setting.png'

export default function Header(){
    return(
        <S.Container>
            <S.Imagem source={logo}/>
            <S.Imagem source={setting}/>
            
        </S.Container>
    )
}