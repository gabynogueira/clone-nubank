import { Text, View } from "react-native";
import * as S from "./styles";

import masterCard from '../../assets/img/mastercard.png';

export default function Cartao(){
    return(
        <S.Container>
            <S.Topo>
                <S.Bandeira source={ masterCard }/>
            </S.Topo>

                <S.Text> Gabriela Nogueira </S.Text>
            
        </S.Container>
    )
}