import styled from "styled-components/native";

export const Container = styled.View`
    width: 100%;
    height: 190px;
    margin-bottom: 20px;
    padding: 20px;
    border-radius: 20px;
    justify-content: space-between;

    background: ${({ theme })=> theme.COLORS.BACKGROUND_CARTAO};
    
    
`;

export const Topo = styled.View`
    align-items: flex-end;
`;

export const Bandeira = styled.Image`

`;

export const Text = styled.Text`
    font-size: 24px;
    font-weight: 600;
    line-height: 36px;
    
    
    color: ${({ theme }) => theme.COLORS.TEXT}

`;
