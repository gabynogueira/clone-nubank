import { Text, View } from "react-native";
import * as S from "./styles";

import pix from '../../assets/img/pix.png';
import { Logo } from './styles';

type Props = {
    logo: any;
    descricao: string;

}

export default function Ajuda({logo, descricao }: Props){
    return(
        <S.Container>
          
            <S.Logo source={logo}/>
            
                <S.Txt>
                    {descricao}
                </S.Txt>
            
            

        </S.Container>
    )
}