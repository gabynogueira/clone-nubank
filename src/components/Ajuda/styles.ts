import styled from "styled-components/native";

export const Container = styled.View`
    width: 100px;
    height: 127px;
    border-radius: 20px;
    padding: 20px;
    align-items: center;
    justify-content: space-around;
    margin-left: 10px;


    background: ${({ theme })=> theme.COLORS.BACKGROUND_CARTAO};
    
`;
export const Logo = styled.Image`

`;

export const Txt = styled.Text`
    font-size: 12px;
    font-weight: 600;
    
    
    color: ${({theme})=>theme.COLORS.TEXT};
`;




