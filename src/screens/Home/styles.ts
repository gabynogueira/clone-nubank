import styled from "styled-components/native";


export const Container = styled.View`
    flex: 1;
    padding-left: 30px;
    padding-right: 30px;

    background: ${({ theme })=> theme.COLORS.BACKGROUND};

`;

export const Text = styled.Text`
     padding-top: 30px;
     padding-left: 20px;
     font-weight: 600;
     padding-bottom: 30px;
     font-size: 14px;
     

     color: ${({ theme }) => theme.COLORS.TEXT};
`;
export const ScrollAjuda = styled.ScrollView`
  
`;
export const ScrollGeral = styled.ScrollView`
   /* padding-bottom: 150px; */

`;
