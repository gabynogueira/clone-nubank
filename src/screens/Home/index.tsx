
import { View } from "react-native";
import Ajuda from "../../components/Ajuda";
import Cartao from "../../components/Cartao ";
import CartaoDetalhes from "../../components/CartaoDetalhes";
import Header from "../../components/Header";

import * as S  from "./styles";

import pix from '../../assets/img/pix.png'
import boleto from '../../assets/img/boleto.png'
import dinheiro from '../../assets/img/dinheiro.png'



export default function Home(){
    return(

        <S.Container>
            <Header/>
            
            <S.ScrollGeral
                showsVerticalScrollIndicator={false}
            >
                <Cartao/>
                <CartaoDetalhes/>

                <S.Text>
                    Do que precisa?
                </S.Text>

        
                <S.ScrollAjuda 
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                >
                    <Ajuda 
                        logo={pix}
                        descricao="fazer pix"
                    />

                    <Ajuda 
                        logo={boleto}
                        descricao="Fazer Pagamennto no Boleto"
                    />

                    <Ajuda 
                        logo={dinheiro}
                        descricao="Pagar em Dinheiro"
                    />

                   
                </S.ScrollAjuda>

                <View style={{  paddingBottom: 60}} />
            </S.ScrollGeral>
        
        </S.Container>
    )
} 